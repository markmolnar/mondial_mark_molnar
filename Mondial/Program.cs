﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Mondial
{
    class Program
    {

        /// <summary>
        /// Első feladat megoldása.
        /// </summary>
        /// <param name="xdocument"></param>
        public static void PopulationDensity(XDocument xdocument)
        {
            var q1 = from x in xdocument.Descendants("country")
                     select new
                     {
                         Orszag = x.Attribute("name").Value,
                         Nepsuruseg = int.Parse(x.Attribute("population").Value) / int.Parse(x.Attribute("total_area").Value)
                     };
            Console.WriteLine("--------------");
            Console.Write("1, A legnagyobb népsűrűségű ország: ");
            var result = q1.OrderByDescending(x => x.Nepsuruseg).First();
            Console.WriteLine(result.Orszag + "->" + result.Nepsuruseg);
        }


        /// <summary>
        /// Második feladat megoldása.
        /// </summary>
        /// <param name="xdocument"></param>
        public static void DemocraticCountries(XDocument xdocument)
        {
            var q2 = from x in xdocument.Descendants("country")
                     where (x.Attribute("government").Value.Contains("democra"))
                     select (x);

            var q2b = from x in xdocument.Descendants("country")
                      select (x);
            Console.WriteLine("--------------");
            Console.Write("2, Demokratikus országok százaléka: ");
            double percentage = ((double)q2.Count() / (double)q2b.Count()) * 100.0;
            Console.WriteLine(percentage+"%");
        }

        /// <summary>
        /// Harmadik feladat megoldása.
        /// </summary>
        /// <param name="xdocument"></param>
        public static void ReligionsEthnics(XDocument xdocument)
        {
            var q3 = from x in xdocument.Descendants("country")
                      select new
                      {
                          Orszag=x.Attribute("name").Value,
                          Vallas=x.Elements("religions").Count(),
                          Etnikum=x.Elements("ethnicgroups").Count()
                      };
            Console.WriteLine("--------------");
            foreach (var item in q3.OrderByDescending(x=>(x.Vallas+x.Etnikum)))
            {
                StringBuilder strBuilder = new StringBuilder();
                string result=strBuilder.Append("Ország:").Append(item.Orszag).Append('\t').Append("Vallások száma:").Append(item.Vallas)
                    .Append('\t').Append("Etnikumok száma:").Append(item.Etnikum).ToString();
                Console.WriteLine(result);
            }
        }

        /// <summary>
        /// Negyedik feladat megoldása.
        /// </summary>
        /// <param name="xdocument"></param>
        public static void BorderLength(XDocument xdocument)
        {
            var q4 = from x in xdocument.Descendants("country")
                      select new
                      {
                          Orszag = x.Attribute("name").Value,
                          Hatar=x.Elements("border").Sum(y=>double.Parse(y.Attribute("length").Value.Replace('.',',')))
                      };
            Console.WriteLine("--------------");
            Console.WriteLine("Határok hossza országonként:");
            foreach (var item in q4.OrderByDescending(x=>x.Hatar))
            {
                Console.WriteLine(item.Orszag+":"+item.Hatar);
            }
        }


        /// <summary>
        /// Ötödik feladat megoldása.
        /// </summary>
        /// <param name="xdocument"></param>
        public static void MostRiver(XDocument xdocument)
        {
            var q5 = from x in xdocument.Descendants("located")
                     where x.Parent.Name == "river"
                     group x by x.Parent.Attribute("name").Value into g
                     select new
                     {
                         Folyo = g.Key,
                         Szam = g.Select(x => x.Attribute("country").Value).Distinct().Count()
                     };
            Console.WriteLine("-----------------");
            Console.WriteLine("Legtöbb országot metsző öt folyó:");
            foreach (var item in q5.OrderByDescending(x => x.Szam).Take(5))
            {
                Console.WriteLine(item.Folyo + ":" + item.Szam);
            }

        }


        /// <summary>
        /// Hatodik feladat megoldása.
        /// </summary>
        /// <param name="xdocument"></param>
        public static void MostWateredCountries(XDocument xdocument)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            var idkNevek = from x in xdocument.Descendants("country")
                           select new
                           {
                               ID = x.Attribute("id").Value,
                               Name = x.Attribute("name").Value
                           };
            foreach (var e in idkNevek.Distinct())
            {
                dict.Add(e.ID, e.Name);
            }

            var q6 = from x in xdocument.Descendants("located")
                     where x.Parent.Name == "river" || x.Parent.Name == "sea" || x.Parent.Name == "lake"
                     select new
                     {
                         Orszag = x.Attribute("country").Value,
                         Folyo = x.Parent.Attribute("name").Value
                     };
            var q6b = from x in q6
                      group x by x.Folyo into g
                      select new
                      {
                          Folyo = g.Key,
                          Orszag = g.Select(x => x.Orszag).Distinct()
                      };
            Console.WriteLine("-----------------");
            Console.WriteLine("Legtöbb folyót,tavat,tengert tartalmazó országok:");
            var q6c = q6b.SelectMany(x => x.Orszag);
            var q6d = from x in q6c
                      group x by x into g
                      select new
                      {
                          Orszag = g.Key,
                          Darab = g.Count()
                      };
            foreach (var item in q6d.OrderByDescending(x => x.Darab).Take(5))
            {
                Console.WriteLine(dict[item.Orszag] + ":" + item.Darab);
            }

        }

        /// <summary>
        /// Hetedik feladat megoldása.
        /// </summary>
        /// <param name="xdocument"></param>
        public static void RiverToSea(XDocument xdocument)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            var idkNevek = from x in xdocument.Descendants("sea")
                           select new
                           {
                               ID = x.Attribute("id").Value,
                               Name = x.Attribute("name").Value
                           };
            foreach (var e in idkNevek.Distinct())
            {
                dict.Add(e.ID, e.Name);
            }

            var q7 = from x in xdocument.Descendants("river")
                     where x.Element("to").Attribute("type").Value == "sea"
                     select (x.Element("to").Attribute("water").Value);

            /*foreach (var item in q7)
            {
                Console.WriteLine(item);
            }*/
            var q7b = from x in q7
                      group x by x into g
                      select new
                      {
                          Id = g.Key,
                          Darab = g.Count()
                      };
            Console.WriteLine("--------------");
            Console.WriteLine("Ezen tengerekbe ömlik a legtöbb folyó: ");
            foreach (var item in q7b.OrderByDescending(x=>x.Darab).Take(5))
            {
                Console.WriteLine(dict[item.Id]+":"+item.Darab);
            }
            
        }

        /// <summary>
        /// Nyolcadik feladat megoldása.
        /// </summary>
        /// <param name="xdocument"></param>
        public static void EthnicSize(XDocument xdocument)
        {
            var q8 = from x in xdocument.Descendants("ethnicgroups")
                     select new
                     {
                         Nev = x.Value,
                         Szam = (double)((double.Parse(x.Attribute("percentage").Value.Replace('.',','))/100)*double.Parse(x.Parent.Attribute("population").Value))
                     };
            var q8b = from x in q8
                      group x by x.Nev into g
                      select new
                      {
                          Nev = g.Key,
                          Nepesseg = g.Sum(x=>x.Szam)
                      };
            Console.WriteLine("--------------");
            Console.WriteLine("Világ etnikumainak a mérete:");
            foreach (var item in q8b.OrderByDescending(x=>x.Nepesseg))
            {
                Console.WriteLine(item.Nev + ":" + item.Nepesseg);
            }
        }



        static void Main(string[] args)
        {
            XDocument xdocument = XDocument.Load("http://www.szabozs.hu/_DEBRECEN/kerteszg/mondial-3.0.xml");
            PopulationDensity(xdocument);
            DemocraticCountries(xdocument);
            ReligionsEthnics(xdocument);
            BorderLength(xdocument);
            MostRiver(xdocument);
            /*MostWateredCountries(xdocument);
            RiverToSea(xdocument);
            EthnicSize(xdocument);*/
            Console.ReadKey();
        }
    }
}
